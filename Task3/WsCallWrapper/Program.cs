﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace WsCallWrapper
{
    public class Program
    {
        private const string Url = "https://www.metaweather.com/api/location/862592";

        static void Main(string[] args)
        {
            Console.WriteLine(CallWebApiAsync<object>(Url).GetAwaiter().GetResult());
        }

        private static async Task<T> CallWebApiAsync<T>(string url) where T : class
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var response = await client.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsAsync<T>();
                }
                else
                {
                    Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                    return null;
                }
            }
        }
    }
}
